module ElAx
  # Configuration for the client to work.
  class Configuration
    # Username/customer number for authentication
    # @return [String]
    attr_accessor :username
    # Password/meter number for authentication
    # @return [String]
    attr_accessor :password
    # Turn off built-in rate-limiter
    # @return [Boolean]
    attr_accessor :dont_rate_limit_automatically

    def initialize
      self.dont_rate_limit_automatically = false
    end
  end
end
