require 'rest-client'
require 'json'
require 'digest/sha2'

module ElAx
  # Internal client to access el.ax
  class ApiClient
    @session_cookies = HTTP::CookieJar.new
    @consumption_endpoint = 'forbrukning'
    @client_resource = nil
    @authenticated = false

    # Create a new instance.
    def initialize
      @endpoint = 'https://minsida.el.ax'
      @user = ElAx.configuration.username
      @password = ElAx.configuration.password
      @authenticated = false
      @updated = {}
      @cached = {}
    end

    # Set the username to authenticate with
    # @return [String]
    attr_writer :user

    # Set the password to authenticate with
    # @return [String]
    attr_writer :password

    attr_reader :updated
    attr_reader :cached

    # Perform authentication.
    def authenticate!
      setup_client
      signin_response = @client_resource['index.con'].post(
        kundnr: @user,
        password: @password
      )
      @session_cookies = signin_response.cookie_jar

      @authenticated = signin_response.body.length > 400
    end

    # Perform de-authentication.
    def deauthenticate!
      setup_client(true)
      @client_resource['logout'].get if authenticated?
      @session_cookies = HTTP::CookieJar.new
      @authenticated = false
    end

    # Tell if authenticated.
    # @return [Boolean] +true+ or +false+
    def authenticated?
      @authenticated
    end

    # Get consumption data for a given spot
    # @param [String] spot the meter ID.
    # @param [Hash] options
    # @option :res år, månad, dag, timme
    # @return [ElAx::Consumption] consumption data
    def get_consumption(spot, options = nil)
      authenticate! unless authenticated?
      setup_client(true)
      request = request_data(spot, options)
      digest = Digest::SHA256.hexdigest(request.to_s)

      # Rate-limiter action
      response = if @updated.key?(digest)
                   return_new_or_cached_data request
                 else
                   perform_query(request, digest)
                 end

      ElAx::Consumption.from_json(response)
    end

    private

    def the_cache_is_old_for_entry(digest)
      if ElAx.configuration.dont_rate_limit_automatically
        true
      else
        @updated[digest] > Time.now.to_i + 300
      end
    end

    def return_new_or_cached_data(data)
      digest = Digest::SHA256.hexdigest(data.to_s)
      if the_cache_is_old_for_entry digest
        perform_query(data, digest)
      else
        @cached[digest]
      end
    end

    def perform_query(data, digest)
      @updated[digest] = Time.now.to_i
      consumption_response = @client_resource['service.con'].get(
        params: {
          f: 'getConsumption',
          data: data
        }
      )

      response = consumption_response.body
      @cached[digest] = response
      response
    end

    def request_data(spot, options = nil)
      data = {
        com: false,
        history: false,
        spot: spot,
        res: 'dag'
      }
      data.merge!(options) unless options.nil?
      data
    end

    def setup_client(with_cookies = false)
      unless with_cookies
        @client_resource = RestClient::Resource.new(
          @endpoint,
          verify_ssl: OpenSSL::SSL::VERIFY_NONE
        )
      end
      if with_cookies
        @client_resource = RestClient::Resource.new(
          @endpoint,
          verify_ssl: OpenSSL::SSL::VERIFY_NONE,
          cookies: @session_cookies
        )
      end
    end
  end
end
