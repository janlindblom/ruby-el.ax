require 'date'

module ElAx
  class Consumption
    # A consumption data entry.
    class Entry
      # @return [String]
      attr_accessor :meter
      # @return [Date]
      attr_accessor :date
      # @return [Date]
      attr_accessor :date_full
      # @return [Time]
      attr_accessor :date_time
      # @return [Date]
      attr_accessor :date_join
      # @return [Date]
      attr_accessor :date_high
      # @return [Float]
      attr_accessor :peak_high
      # @return [Date]
      attr_accessor :peak_high_date
      # @return [Integer]
      attr_accessor :quality_high
      # @return [Float]
      attr_accessor :meter_high
      # @return [Integer]
      attr_accessor :state_high
      # @return [Float]
      attr_accessor :data_high
      # @return [Integer]
      attr_accessor :peak_low
      # @return [Date]
      attr_accessor :peak_low_date
      # @return [Integer]
      attr_accessor :data_low
      # @return [Integer]
      attr_accessor :meter_low
      # @return [Float]
      attr_accessor :temp
      # @return [Date]
      attr_accessor :start
      # @return [Date]
      attr_accessor :end
      # @return [Integer]
      attr_accessor :counters
      # @return [String]
      attr_accessor :spot
      # @return [String]
      attr_accessor :number
      # @return [Integer]
      attr_accessor :factor
      # @return [Integer]
      attr_accessor :limited
      # @return [Boolean]
      attr_accessor :compare
      # @return [Integer]
      attr_accessor :compare_id
      # @return [Integer]
      attr_accessor :year

      # Specials

      # @return [Integer]
      attr_accessor :constant
      # @return [String]
      attr_accessor :type

      # Create a consumption data entry, optionally from a Hash.
      #
      # @param [Hash] hash optional hash with existing data.
      def initialize(hash = nil)
        return if hash.nil?
        hash.each do |k, v|
          key = k
          value = v
          key = :compare_id if k == :compareId
          key = :constant if k == :Konstant
          value = based_on_key(key, value)

          public_send("#{key}=", value)
        end
      end

      # Return a Hash representation of this instance.
      #
      # @see #to_h
      # @return [Hash]
      def to_hash
        to_h
      end

      # Return a Hash representation of this instance.
      #
      # @return [Hash]
      def to_h
        data = {}
        keys.each do |k|
          val = public_send(k.to_s)
          data[k] = val unless val.nil?
        end
        data
      end

      # Return the JSON String representation of this instance.
      # @return [String]
      def to_json
        to_h.to_json
      end

      # Return the String representation of this instance.
      # @return [String]
      def to_s
        to_h.to_s
      end

      private

      def based_on_key(key, value)
        case key
        when :date, :date_full, :date_high, :date_join, :start, :end
          value = parse_to_date(value)
        when :date_time, :peak_high_date, :peak_low_date
          value = parse_to_datetime(value)
        when :constant, :quality_high, :state_high
          value = Integer value
        end
        value
      end

      def parse_to_date(d)
        Date.parse d
      rescue ArgumentError
        d
      end

      def parse_to_datetime(d)
        Time.parse "#{d} EET"
      rescue ArgumentError
        d
      end

      def keys
        %i[meter
           date
           date_full
           date_join
           date_time
           date_high
           peak_high
           peak_high_date
           quality_high
           meter_high
           state_high
           constant
           data_high
           type
           peak_low
           peak_low_date
           data_low
           meter_low
           temp
           start
           end
           counters
           spot
           number
           factor
           limited
           compare
           compare_id
           year]
      end
    end
  end
end
