require 'el_ax/consumption/entry'

module ElAx
  # A representation of the electricity consumption for a spot.
  class Consumption
    # Consumption entries.
    # @return [Array<ElAx::Consumption::Entry>]
    attr_reader :entries
    # Timestamp of this instance
    # @return [Integer]
    attr_reader :timestamp

    # Create a new instance.
    #
    # @param [Array<Hash>] entries optional initial entries to process
    def initialize(entries = [])
      @timestamp = Time.now.to_i
      return if entries.empty?
      @entries = entries.map { |e| ElAx::Consumption::Entry.new e }
    end

    # Create a new instance from a JSON String.
    #
    # @param [String] json_data a JSON String
    # @return [Consumption] instance created from a JSON String
    def self.from_json(json_data)
      require 'json'
      data = JSON.parse(json_data, symbolize_names: true)
      ElAx::Consumption.new(data)
    end

    # Return the String representation of this instance.
    # @return [String]
    def to_s
      to_h.to_s
    end

    # Return the Hash representation of this instance.
    # @see #to_h
    # @return [Hash]
    def to_hash
      to_h
    end

    # Return the Hash representation of this instance.
    # @return [Hash]
    def to_h
      {
        timestamp: timestamp,
        entries: entries.map(&:to_h)
      }
    end

    # Return the JSON String representation of this instance.
    # @return [String]
    def to_json
      to_h.to_json
    end

    # Return an {Array<Hash>} representation of this instance.
    # @return [Array<Hash>]
    def to_a
      entries.map(&:to_h)
    end
  end
end
