require 'el_ax/version'
require 'el_ax/configuration'
require 'el_ax/api_client'
require 'el_ax/consumption/entry'
require 'el_ax/consumption'

# Access el.ax programmatically.
#
# @author Jan Lindblom <jan@robotika.ax>
module ElAx
  # Modify the current configuration.
  #
  # @example
  #   ElAx.configure do |config|
  #     config.username = "YOUR_PASSWORD"
  #     config.password = "YOUR_PASSWORD"
  #     config.dont_rate_limit_automatically = false
  #   end
  #
  # @yieldparam [ElAx::Configuration] config current configuration
  def self.configure
    self.configuration ||= ElAx::Configuration.new
    yield self.configuration
  end

  class << self
    # Access the configuration object.
    attr_accessor :configuration

    # Get electricity consumption for a given spot.
    #
    # @param [String] spot the spot to get consumption for
    # @return [ElAx::Consumption] consumption data for given spot
    def get_consumption(spot)
      c = ElAx::ApiClient.new
      c.authenticate!
      data = c.get_consumption(spot)
      c.deauthenticate!
      data
    end
  end
end
