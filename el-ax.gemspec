# -*- encoding: utf-8 -*-
lib = File.expand_path('lib', __dir__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'el_ax/version'

Gem::Specification.new do |spec|
  spec.name          = 'el-ax'
  spec.version       = ElAx::VERSION
  spec.authors       = ['Jan Lindblom']
  spec.email         = ['jan@robotika.ax']

  spec.summary       = 'Interface to el.ax.'
  spec.description   = 'Access information about your electrical consumption' \
                       ' with Ålands Elandelslag programatically.'
  spec.homepage      = 'https://bitbucket.org/janlindblom/ruby-el.ax/'
  spec.license       = 'MIT'

  spec.bindir        = 'exe'
  spec.files = `git ls-files -z`.split("\x0").reject { |f|
    f.match(%r{^(test|spec|features|bitbucket-pipelines.yml|.rspec|bin)/|.rspec|bitbucket-pipelines.yml|.travis.yml|.editorconfig|Rakefile})
  }
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.14'
  spec.add_development_dependency 'dotenv', '~> 2.5'
  spec.add_development_dependency 'pry', '~> 0.11'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rubocop', '~> 0.58'
  spec.add_development_dependency 'yard', '~> 0.9'
  spec.add_development_dependency 'simplecov', '~> 0.16'
  spec.add_runtime_dependency 'rest-client', '~> 2.0'
end
