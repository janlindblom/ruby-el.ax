RSpec.describe ElAx::Consumption::Entry do
  describe 'with proper configuration', configured: true, authenticated: true do
    before :all do
      @c = ElAx::ApiClient.new
      @c.authenticate!
      data = @c.get_consumption(ENV['ELEX_SPOT'])
      @c.deauthenticate!
      @entry = data.entries.first
    end

    it 'can return a hash with the data contained in the entry' do
      expect(@entry.to_h).to be_a Hash
      expect(@entry.to_hash).to be_a Hash
    end

    it 'can return a JSON string with the data contained in the entry' do
      expect(@entry.to_json).to be_a String
      expect(@entry.to_s).to be_a String
    end
  end
end
