RSpec.describe ElAx::ApiClient, authenticated: true do
  before :all do
    @c = ElAx::ApiClient.new
  end

  it 'can properly log in to el.ax' do
    @c.authenticate!
    expect(@c.authenticated?).to be true
    @c.deauthenticate!
  end

  it 'can download consumption data from el.ax' do
    @c.authenticate!
    data = @c.get_consumption(ENV['ELEX_SPOT'])
    expect(data).not_to be nil
    expect(data).to be_a ElAx::Consumption
    expect(data.entries).not_to be_empty
    @c.deauthenticate!
  end

  it 'will return a cached response if calling again' do
    @c.authenticate!
    data = @c.get_consumption(ENV['ELEX_SPOT'])
    expect(data).not_to be nil
    expect(data).to be_a ElAx::Consumption
    expect(data.entries).not_to be_empty
    @c.deauthenticate!
  end
end
