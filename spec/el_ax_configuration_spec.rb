RSpec.describe ElAx::Configuration, configured: true do
  it 'contains a password for authentication', authenticated: true do
    expect(ElAx.configuration.password).to be_a String
  end

  it 'contains a username for authentication', authenticated: true do
    expect(ElAx.configuration.username).to be_a String
  end
end
