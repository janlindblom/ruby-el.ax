RSpec.describe ElAx do
  it 'has a version number' do
    expect(ElAx::VERSION).not_to be nil
  end

  it 'contains a Configuration object', configured: false do
    expect(ElAx.configuration).to be_a ElAx::Configuration
  end

  it 'responds to #get_consumption', configured: false, authenticated: false do
    expect(ElAx).to respond_to :get_consumption
  end

  describe '#get_consumption' do
    it 'can download consumption data from el.ax',
      authenticated: true,
      configured: true do
      data = ElAx.get_consumption(ENV['ELAX_SPOT'])
      expect(data).not_to be nil
      expect(data).to be_a ElAx::Consumption
    end
  end
end
