require 'bundler/setup'
require 'dotenv/load'
require 'simplecov'
SimpleCov.start do
  add_filter 'spec'
end
require 'el_ax'

ElAx.configure do |config|
  config.username = ENV['ELAX_USERNAME']
  config.password = ENV['ELAX_PASSWORD']
  config.dont_rate_limit_automatically = false
end

RSpec.configure do |config|
  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = '.rspec_status'

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
