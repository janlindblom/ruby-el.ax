# Ruby el.ax

Access information about electricity consumption from Ålands Elandelslag
(el.ax) with Ruby!

This is an unofficial client for accessing the API exposed through the el.ax
website. The API is in no way meant to be accessed this way, it's only meant
to be used by the interface available for customers to look at their usage
data.

### Disclaimer

**Before you continue:** this is a notice to inform you that I take no
responsibility for this gem to work at any time. It is neither supported,
endorsed or even acknowledged by Ålands Elandelslag. Please _use responsibly,_
_rate-limit your usage_ and _don't do bad stuff._

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'el-ax'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install el-ax

## Usage

Load the library and configure the client like so:

```ruby
require 'el_ax'

ElAx.configure do |config|
  config.username = 'YOUR_USERNAME'
  config.password = 'YOUR_PASSWORD'
  # The client will do basic rate-limiting, turn if off if you roll your own
  #config.dont_rate_limit_automatically = false
end
```

Then use the `get_consumption` method like so:

```ruby
ElAx.get_consumption('YOUR_SPOT_NUMBER')
```

See the documentation for more details on what the response looks like.

### A note on rate-limiting

The library will default to rate-limit your queries to roughly one query every
five minutes by caching the response to each query internally. This can be
turned off by setting the `dont_rate_limit_automatically` in the configuration.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://bitbucket.org/janlindblom/ruby-el.ax. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the ElAx project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://bitbucket.org/janlindblom/ruby-el.ax/src/master/CODE_OF_CONDUCT.md).
